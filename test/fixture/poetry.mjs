#!/usr/bin/env node

import {exit, env, argv} from 'node:process';
import {readFile} from 'node:fs/promises';
import {parse} from '@ltd/j-toml';

if (typeof env.FIXTURE_POETRY_FORCE_STDERR === 'string') {
	console.warning(env.FIXTURE_POETRY_FORCE_STDERR);
	exit(1);
}

function failure(message) {
	console.log(`  SomeException\n\n  ${message}\n\n  at /some/path`);
	exit(1);
}

const data = await (async () => {
	try {
		return await readFile('pyproject.toml');
	} catch (error) {
		if (error.code === 'ENOENT') {
			failure('`pyproject.toml` did not exist');
		}

		throw error;
	}
})();

const toml = parse(data);

if (argv.includes('check')) {
	if (toml.tool === undefined) {
		failure('`tool` was not defined');
	}

	if (toml.tool.poetry === undefined) {
		failure('`tool.poetry` was not defined');
	}

	const missing = ['name', 'version', 'description'].filter(
		k => toml.tool.poetry[k] === undefined,
	);

	if (missing.length > 0) {
		console.log(missing.map(k => `Error: '${k} is missing'`).join('\n'));
		exit(1);
	}

	exit(0);
}
