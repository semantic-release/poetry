// eslint-disable-next-line unicorn/prefer-module
const test = require('ava');

test('Can load the CommonJS module', async t => {
	// eslint-disable-next-line unicorn/prefer-module
	const {verifyConditions, prepare, publish} = require('../plugin.js');

	t.is(typeof verifyConditions, 'function');
	t.is(typeof prepare, 'function');
	t.is(typeof publish, 'function');

	const cfg = {poetry: '/does/not/exist'};
	const ctx = {};

	await t.throwsAsync(verifyConditions(cfg, ctx));
	await t.throwsAsync(prepare(cfg, ctx));
	await t.throwsAsync(publish(cfg, ctx));
});
