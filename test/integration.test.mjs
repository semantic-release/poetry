import path from 'node:path';
import {readFile, writeFile} from 'node:fs/promises';
import {env} from 'node:process';
import http from 'node:http';
import {URL} from 'node:url';
import {randomUUID} from 'node:crypto';
import test from 'ava';
import {stub} from 'sinon';
import clearModule from 'clear-module';
import {WritableStreamBuffer} from 'stream-buffers';
import {temporaryDirectory} from 'tempy';
import express from 'express';
import listen from 'test-listen';
import which from 'which';
import got from 'got';
import {template} from 'lodash-es';

const URI = new URL(import.meta.url);
const SCRIPT = URI.pathname;
const DIRNAME = path.dirname(SCRIPT);

test.beforeEach(async t => {
	clearModule('../plugin.mjs');
	t.context.m = await import('../plugin.mjs');

	t.context.middleware = stub().callsFake((request, response) => {
		const {'private-token': token, authorization: auth} = request.headers;
		if (token !== undefined) {
			response.status(Number.parseInt(token, 10));
		} else if (auth.startsWith('Basic')) {
			response.status(400);
		}

		response.json({});
	});
	t.context.app = express();
	t.context.app.post('/', t.context.middleware);
	t.context.server = http.createServer(t.context.app);
	t.context.url = await listen(t.context.server);

	t.context.cfg = {
		repositories: {
			// eslint-disable-next-line no-template-curly-in-string
			localhost: '${LOCALHOST}',
		},
		poetry: path.join(DIRNAME, 'fixture', 'poetry.mjs'),
	};

	const environ = Object.fromEntries(Object.entries(env));
	environ.LOCALHOST = t.context.url;
	environ.POETRY_PYPI_TOKEN_LOCALHOST = '400';

	t.context.log = stub();
	t.context.ctx = {
		cwd: temporaryDirectory(),
		env: environ,
		options: {},
		stdout: new WritableStreamBuffer(),
		stderr: new WritableStreamBuffer(),
		logger: {
			log: t.context.log,
			success: t.context.log,
			warn: t.context.log,
		},
	};

	t.context.toml = path.join(t.context.ctx.cwd, 'pyproject.toml');
	await writeFile(
		t.context.toml,
		'[tool.poetry]\nname=""\nversion=""\ndescription=""\nauthors=[]',
	);
});

const failure = test.macro({
	// eslint-disable-next-line max-params
	async exec(t, code, regex, before, after) {
		if (before) {
			await before(t);
		}

		const error = await t.throwsAsync(
			t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
		);

		t.is(error.name, 'SemanticReleaseError', `${error}`);
		t.is(error.code, code, `${error}`);
		t.regex(error.message, regex, `${error}`);

		if (after) {
			await after(t);
		}
	},
	title: (message, code, regex) =>
		message ?? `Throws \`${code}\` matching ${regex.source}`,
});

const success = test.macro({
	async exec(t, before, after) {
		if (before) {
			await before(t);
		}

		await t.notThrowsAsync(
			t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
		);
		t.context.ctx.nextRelease = {version: '1.0.0'};
		await t.notThrowsAsync(t.context.m.prepare(t.context.cfg, t.context.ctx));
		await t.notThrowsAsync(t.context.m.publish(t.context.cfg, t.context.ctx));

		if (after) {
			await after(t);
		}
	},
	title: message =>
		message ? `Successfully runs with ${message}` : 'Successfully runs',
});

test(failure, 'EPOETRYCFG', /`repositories` must be a mapping/, async t => {
	t.context.cfg.repositories = [];
});

test(failure, 'EPOETRYCFG', /`localhost` URL was not a string but a `number`/, async t => {
	t.context.cfg.repositories.localhost = 1;
});

test(failure, 'EPOETRYCFG', /Failed to do environment variable replacement on `.+`/, async t => {
	// eslint-disable-next-line no-template-curly-in-string
	t.context.cfg.repositories.localhost = '${NOPE}';
});

test(failure, 'EPOETRYCFG', /Failed to parse `.+` as a URL/, async t => {
	t.context.cfg.repositories.localhost = '.';
});

test(failure, 'EPOETRYCLI', /Poetry CLI is not available at `.+`/, async t => {
	t.context.cfg.poetry = '/this/does/not/exist';
});

test(failure, 'EPOETRYCLI', /`.+ check .+` unexpectedly wrote to `stderr`/, async t => {
	t.context.ctx.env.FIXTURE_POETRY_FORCE_STDERR = 'yes, please';
});

test(failure, 'EPOETRYCLI', /`.+ check .+` failed/, async t => {
	await writeFile(t.context.toml, '');
});

test(failure, 'EPOETRYCLI', /`.+ check .+` found errors/, async t => {
	await writeFile(t.context.toml, '[tool.poetry]\nname=""\nversion=""');
});

test(failure, 'EPOETRYAUTH',	/Failed to find `.+` token in the environment/,	async t => {
	delete t.context.ctx.env.POETRY_PYPI_TOKEN_LOCALHOST;
});

test(failure, 'EPOETRYAUTH', /Failed to authenticate `.+`/, async t => {
	t.context.ctx.env.POETRY_PYPI_TOKEN_LOCALHOST = '401';
});

test(failure, 'EPOETRYAUTH', /Failed to POST to `.+` \(499\)/, async t => {
	t.context.ctx.env.POETRY_PYPI_TOKEN_LOCALHOST = '499';
});

test(success);

test('no repositories', success, async t => {
	delete t.context.cfg.repositories;
});

test('no repository items', success, async t => {
	delete t.context.cfg.repositories.localhost;
});

test('username/password', success, async t => {
	delete t.context.ctx.env.POETRY_PYPI_TOKEN_LOCALHOST;
	t.context.ctx.env.POETRY_HTTP_BASIC_LOCALHOST_USERNAME = 'placeholder';
	t.context.ctx.env.POETRY_HTTP_BASIC_LOCALHOST_PASSWORD = 'placeholder';
});

test('PyPI repository', success, async t => {
	t.context.cfg.repositories.pypi = t.context.cfg.repositories.localhost;
	delete t.context.cfg.repositories.localhost;

	t.context.ctx.env.POETRY_PYPI_TOKEN_PYPI = t.context.ctx.env.POETRY_PYPI_TOKEN_LOCALHOST;
	delete t.context.ctx.env.POETRY_PYPI_TOKEN_LOCALHOST;
});

const testIf = (
	which.sync('poetry')
  && env.CI_API_V4_URL
  && env.CI_PROJECT_ID
  && env.POETRY_PYPI_TOKEN_GITLAB // TODO: Enable this token once the package registry is enabled
) ? test : test.skip;

testIf('real Poetry CLI', success, async t => {
	delete t.context.cfg.repositories.localhost;
	delete t.context.ctx.env.POETRY_PYPI_TOKEN_LOCALHOST;
	delete t.context.cfg.poetry;

	t.context.ctx.cwd = path.join(DIRNAME, 'fixture', 'pkg');

	// eslint-disable-next-line no-template-curly-in-string
	t.context.cfg.repositories.gitlab = '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi';

	t.context.toml = {path: path.join(t.context.ctx.cwd, 'pyproject.toml'), name: `test-${randomUUID()}`};
	t.context.toml.data = await readFile(t.context.toml.path, {encoding: 'utf8'});
	const replaced = t.context.toml.data.replace(/(name\s*=\s*)".+"/, `$1"${t.context.toml.name}"`);
	await writeFile(t.context.toml.path, replaced);
}, async t => {
	// We have to remove any published packages
	// Get a `400: bad request` otherwise when doing the next test
	const url = new URL(template(t.context.cfg.repositories.gitlab)(env));
	url.pathname = path.dirname(url.pathname);
	const headers = {'PRIVATE-TOKEN': env.POETRY_PYPI_TOKEN_GITLAB};
	const packages = await got.get(url, {headers}).json();
	const pkg = packages.find(p => p.name === t.context.toml.name);
	url.pathname = path.join(url.pathname, `${pkg.id}`);
	await got.delete(url, {headers}).json();
	await writeFile(t.context.toml.path, t.context.toml.data);
});
