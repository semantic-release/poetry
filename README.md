# @semantic-release/poetry

> [**semantic-release**](https://github.com/semantic-release/semantic-release)
> plugin to publish a [Poetry](https://python-poetry.org) package.

| Step               | Description                                                                                                                                  |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------- |
| `verifyConditions` | Perform Poetry check, verify the presence of the `POETRY_PYPI_TOKEN_*` environment variables, and verify the authentication method is valid. |
| `prepare`          | [Bump the Poetry project version](https://python-poetry.org/docs/cli/#version) and [build](https://python-poetry.org/docs/cli/#build) it.    |
| `publish`          | [Publish the Poetry package](https://python-poetry.org/docs/cli/#publish) to the repositories.                                               |

## Getting Started

```sh
npm config --location project set registry https://gitlab.arm.com/api/v4/groups/semantic-release/-/packages/npm/
npm install --save-dev @semantic-release/poetry
```

Add the following to `.releaserc.yaml`:

```yaml
plugins:
  - path: "@semantic-release/poetry"
    repositories:
      gitlab: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi
```

## Configuration

### `repositories`

The mapping of repository names to PyPI endpoint URLs.

Can use environment variables within the URL via `lodash` templating.

### Authentication

The authentication is found in the environment variables as described by the [Poetry documentation][auth].

Where `$REPO` is the uppercase version of a repository name, provide either a token:

- `POETRY_PYPI_TOKEN_$REPO`

or, a username and password:

- `POETRY_HTTP_BASIC_$REPO_USERNAME`
- `POETRY_HTTP_BASIC_$REPO_PASSWORD`

#### GitLab

Creating a [Deploy Token][deploy-token] with `write_repository` permission is the preferred method to restrict the access.

The deploy token can be use with the Poetry username/password authentication to publish packages.

### `poetry`

The path to the Poetry CLI.

Defaults to `poetry` which will be found on `PATH`.

[auth]: https://python-poetry.org/docs/repositories#configuring-credentials
[deploy-token]: https://docs.gitlab.com/ee/user/project/deploy_tokens/
