import {URL} from 'node:url';
import SemanticReleaseError from '@semantic-release/error';
import dbg from 'debug';
import {template} from 'lodash-es';
import {execa} from 'execa';
import got from 'got';

const debug = dbg('semantic-release:poetry');

function verbatim(...args) {
	const backticks = '```';
	return args
		.map(a => `${backticks}\n${a}\n${backticks}`)
		.join('\n');
}

async function poetry(pluginConfig, context, ...args) {
	const {env, cwd} = context;
	const {poetry: cli = 'poetry'} = pluginConfig;

	const cmd = args.shift();
	args.unshift('-vvv');
	args.unshift('--no-interaction');
	args.unshift(cmd);

	try {
		debug('Executing %s %o', cli, args);
		await execa(cli, args, {cwd, env});
	} catch (error) {
		if (error.code === 'ENOENT') {
			throw new SemanticReleaseError(
				`Poetry CLI is not available at \`${cli}\``,
				'EPOETRYCLI',
				verbatim(error),
			);
		}

		const cmd = `${cli} ${args.join(' ')}`;

		// Poetry, weirdly, writes errors to the standard output
		if (error.stderr) {
			throw new SemanticReleaseError(
				`\`${cmd}\` unexpectedly wrote to \`stderr\``,
				'EPOETRYCLI',
				verbatim(error.stdout, error.stderr),
			);
		}

		// Poetry uses Cleo to print thrown errors
		// Attempt to parse the output to get the "real" reason
		const stdout = error.stdout.replaceAll(/^ {2}/gm, '');
		const match = /^.+(?:Exception|Error)\n\n(.+)\n\nat .+/ms.exec(stdout);
		if (match) {
			throw new SemanticReleaseError(
				`\`${cmd}\` failed`,
				'EPOETRYCLI',
				verbatim(match[1]),
			);
		}

		throw new SemanticReleaseError(
			`\`${cmd}\` found errors`,
			'EPOETRYCLI',
			verbatim(error.stdout),
		);
	}
}

export async function verifyConditions(pluginConfig, context) {
	const {env, logger} = context;
	const {repositories = {}} = pluginConfig;

	// Validate `poetry check` works
	await poetry(pluginConfig, context, 'check');
	logger.success('`pyproject.toml` is valid');

	// Make sure the project builds
	await poetry(pluginConfig, context, 'build');
	logger.success('Project builds');

	debug('Validating Poetry repositories: %j', repositories);

	// Make sure we have the correct shape for the repositories
	if (typeof repositories !== 'object' || Array.isArray(repositories)) {
		throw new SemanticReleaseError(
			'`repositories` must be a mapping',
			'EPOETRYCFG',
			verbatim(repositories),
		);
	}

	// Warn if no repositories
	if (Object.keys(repositories).length === 0) {
		logger.warn('No repositories configured');
		return;
	}

	// Validate the repository configuration
	for (let [repository, url] of Object.entries(repositories)) {
		// We need a string to convert to a URL
		if (typeof url !== 'string') {
			throw new SemanticReleaseError(
				`\`${repository}\` URL was not a string but a \`${typeof url}\``,
				'EPOETRYCFG',
			);
		}

		// Do environment variable replacement on the URL
		try {
			url = template(url)(env);
		} catch ({message}) {
			throw new SemanticReleaseError(
				`Failed to do environment variable replacement on \`${url}\``,
				'EPOETRYCFG',
				message,
			);
		}

		// Make sure the URL *is* a URL
		try {
			url = new URL(url);
		} catch ({message}) {
			throw new SemanticReleaseError(
				`Failed to parse \`${url}\` as a URL`,
				'EPOETRYCFG',
				message,
			);
		}

		// Make sure we have the either the token or the username/password
		const key = repository.toUpperCase().replaceAll(/[^A-Z_]+/g, '_');
		debug('Checking for `%s` authentication in environment variables', key);
		const {
			[`POETRY_PYPI_TOKEN_${key}`]: token,
			[`POETRY_HTTP_BASIC_${key}_USERNAME`]: username,
			[`POETRY_HTTP_BASIC_${key}_PASSWORD`]: password,
		} = env;
		if (!(token || (username && password))) {
			throw new SemanticReleaseError(
				`Failed to find \`${key}\` token in the environment`,
				'EPOETRYAUTH',
				`Define a token or username/password in the environment:\n\n  - \`POETRY_PYPI_TOKEN_${key}\`\n\n  - \`POETRY_HTTP_BASIC_${key}_USERNAME\`\n  - \`POETRY_HTTP_BASIC_${key}_PASSWORD\``,
			);
		}

		// Add repository URL
		if (repository !== 'pypi') {
			debug('Adding Poetry `%s` repository URL `%s`', repository, url);
			// eslint-disable-next-line no-await-in-loop
			await poetry(
				pluginConfig,
				context,
				'config',
				`repositories.${repository}`,
				`${url}`,
			);
		}

		// Do auth against API endpoint
		debug('Validating Poetry `%s` repository authentication', repository);
		const options = {throwHttpErrors: false};
		if (token !== undefined) {
			options.headers = {'PRIVATE-TOKEN': token};
		} else if (username !== undefined && password !== undefined) {
			options.username = username;
			options.password = password;
		}

		// eslint-disable-next-line no-await-in-loop
		const {statusCode: code, body} = await got.post(url, options);
		if (code === 400) {
			debug('Assuming `400: Bad Request` is authenticated');
			logger.success('Authenticated `%s` repository', repository);
		} else if (code === 401) {
			throw new SemanticReleaseError(
				`Failed to authenticate \`${repository}\``,
				'EPOETRYAUTH',
				verbatim(body),
			);
		} else {
			throw new SemanticReleaseError(
				`Failed to POST to \`${url}\` (${code})`,
				'EPOETRYAUTH',
				verbatim(body),
			);
		}
	}

	logger.success('Poetry repositories are valid');
}

export async function prepare(pluginConfig, context) {
	const {logger} = context;
	const {nextRelease: {version}} = context;

	await poetry(pluginConfig, context, 'version', `${version}`);
	logger.success('Bumped project version to `%s`', version);

	await poetry(pluginConfig, context, 'build');
	logger.success('Built project for `%s`', version);
}

export async function publish(pluginConfig, context) {
	const {logger, env} = context;
	const {repositories = {}} = pluginConfig;
	env.PYTHON_KEYRING_BACKEND = 'keyring.backends.fail.Keyring';
	for (const repository of Object.keys(repositories)) {
		debug('Publishing to `%s`', repository);
		const args = repository === 'pypi' ? [] : ['--repository', repository];
		// eslint-disable-next-line no-await-in-loop
		await poetry(pluginConfig, context, 'publish', ...args);
		logger.success('Published to `%s`', repository);
	}
}
