async function verifyConditions(pluginConfig, context) {
	const {verifyConditions: shim} = await import('./plugin.mjs');
	return shim(pluginConfig, context);
}

async function prepare(pluginConfig, context) {
	const {prepare: shim} = await import('./plugin.mjs');
	return shim(pluginConfig, context);
}

async function publish(pluginConfig, context) {
	const {publish: shim} = await import('./plugin.mjs');
	return shim(pluginConfig, context);
}

// eslint-disable-next-line unicorn/prefer-module
module.exports = {
	verifyConditions,
	prepare,
	publish,
};
